<?xml version="1.0" ?><html>
	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"/>
		<link rel="stylesheet" href="/source/style.css"/>
		<link rel="stylesheet" href="/syntax-style.css"/>
		<title>AlexRamallo | Kha Tutorial: Part 1</title>
	<meta property="og:title" content="Kha Tutorial: Part 1"/><meta property="og:url" content="https://aramallo.com/kha-tutorial-1.html"/></head>
	<body>
		<div class="menu-tag-2 menu-item">
			AlexRamallo
			 &lt;<a href="mailto:alejandro@ramallo.me">alejandro@ramallo.me</a>&gt;
			 &lt;<a href="https://aramallo.itch.io/">Itch</a>&gt;
			 &lt;<a href="https://twitter.com/AlexRamallo">Twitter</a>&gt;
			 &lt;<a href="/blog/rss.xml">RSS</a>&gt;
		</div>

		<div class="body-wrapper">
		<div class="menu-bar">
			<div id="NavMenu" class="menu-bar-body"><a class="menu-item" href="/">Home</a><a class="menu-item" href="/projects">Projects</a><a class="menu-item" href="/blog">Blog</a><a class="menu-item" href="/About">About</a></div>
		</div>
		<div class="menu-tag-1">
		AlexRamallo &lt;<a href="mailto:alejandro@ramallo.me">alejandro@ramallo.me</a>&gt;
		&lt;<a href="/blog/rss.xml">RSS</a>&gt;
		</div>
		<div class="main-content">
			<article id="MainContent" class="main-content-body"><span><h1>Kha Tutorial: Part 1</h1>
<p class="article-date">Sunday, August 23, 2020 - 12:00 AM</p>

<h2>Kha Overview</h2>
<p>Kha is a Zlib-licensed set of modern low-level APIs written in Haxe. When you compile your Kha project, the Haxe compiler generates code for your chosen platform, and Kha generates/builds your program using the appropriate backend. For example, if you build your project for Windows, then the Kha compiler would use a DirectX backend for its graphics APIs.</p>
<h2>API Organization/Structure</h2>
<p>Kha organizes its APIs based on their type. For example:</p>
<ul>
<li>The &quot;Graphics1&quot; API is an extremely basic one that offers only one operation: <code>setPixel</code></li>
<li>The &quot;Graphics2&quot; API is more advanced, and offers operations like <code>drawImage</code>, <code>drawRect</code>, and even has support for drawing text using truetype fonts. If you've ever used SDL's 2D drawing API, this is similar.</li>
<li>The &quot;Graphics3&quot; API is modeled after the old fixed-function OpenGL and DirectX APIs</li>
<li>The &quot;Graphics4&quot; API is modeled after modern OpenGL, DirectX, etc with shaders.</li>
<li>The &quot;Graphics5&quot; API is modeled after Vulkan, Metal, DirectX12, etc. These are the latest and greatest in the world of graphics APIs, and if you want to be on the cutting edge, this is the one to use.</li>
</ul>
<p>The cool thing is that Kha is smart enough to use its own APIs to implement itself where possible. So, for example, if you're on a modern system that supports OpenGL, then the Graphics2 API functions would actually be using the same backend for Graphics4. Likewise, you can use the Graphics5 backend for Graphics4.</p>
<p>So let's say you want to make a 2D game using the Graphics2 API, and build it for HTML5 so you can put it on your website. Kha will first try to render the game using WebGL (Graphics4) for hardware acceleration, but if that doesn't work then it will fall back to Canvas (Graphics2). All of this happens automatically without you having to modify your game's code!</p>
<h2>Building A Program By Hand</h2>
<p>So enough about APIs, let's make a sample program! There are already a couple of tutorials out there that walk you through using Kha with KodeStudio, which is a distribution of VisualStudio Code that comes bundled with Kha's build system. That's by far the easiest way to get started, because it requires no setup.</p>
<p>But instead of that, I'll go through the process of setting up and running a Kha project using nothing more than GNU Make. If you're on Windows, then you can use <code>nmake</code> which is bundled with Visual Studio, install MinGW/Cygwin/etc, or write the commands manually in Powershell or cmd.exe as they're not complicated at all.</p>
<h3>Required Software</h3>
<p>You first need to install this software if you don't already have it:</p>
<ul>
<li><a href="https://haxe.org">Haxe</a></li>
<li><a href="https://git-scm.com/">Git</a></li>
<li><a href="https://nodejs.org/">NodeJS</a>. This is only required for Kha's custom build system</li>
</ul>
<p>That's it. Just follow standard installation procedures for all of them. In the case of Node, I'm pretty sure it works with either version.</p>
<h3>Setup Development Environment</h3>
<p>You must first clone the Kha repository, as well as another one called Krom. Krom is a special native runtime for Kha designed to make the iteration process as fast as possible. You don't have to use this, but I'll show you how since most people will likely want it.</p>
<h4>Clone Kha</h4>
<div class="codehilite"><pre><span/>$ <span class="nb">cd</span> ~/repos <span class="c1">#make sure you clone it somewhere sane. It's a large repo</span>
$ git clone https://github.com/Kode/Kha.git
$ <span class="nb">cd</span> Kha
$ git submodule update --init --recursive <span class="c1">#clone submodules, which includes the various backends</span>
</pre></div>

<p>This should take a while depending on your internet speed, as there is a lot of stuff to download.</p>
<h4>Clone Krom</h4>
<p>There is an official repo that contains precompiled binaries of Krom, since the build process appears to not be finalized yet. The original code repo <a href="https://github.com/Kode/Krom">can be found here</a></p>
<div class="codehilite"><pre><span/>$ <span class="nb">cd</span> ~/repos <span class="c1">#again, go somewhere sane</span>
$ git clone https://github.com/Kode/Krom_bin.git <span class="c1">#NOTE: this contains precompiled binaries of Krom</span>
</pre></div>

<h3>Project Directory Structure</h3>
<p>A Kha project is actually very simple, and doesn't have many specific structural requirements. All you need is:</p>
<ul>
<li><code>khafile.js</code>, which tells the Kha build system how your project is laid out, dependencies, etc</li>
<li><code>build</code> folder, which is automatically generated when you build your project, but I'm not sure if/how you can rename it.</li>
</ul>
<p>So for the sake of this tutorial, I'll just copy the structure that KodeStudio uses by default:</p>
<div class="codehilite"><pre><span/>MyProject/
  -&gt; Sources/
      -&gt; Main.hx
      -&gt; Project.hx
  -&gt; Assets/
      -&gt; images, audio, fonts, etc
  -&gt; khafile.js
</pre></div>

<h4>khafile.js</h4>
<div class="codehilite"><pre><span/><span class="kd">let</span> <span class="nx">project</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">Project</span><span class="p">(</span><span class="s1">'My Kha Project'</span><span class="p">);</span>
<span class="nx">project</span><span class="p">.</span><span class="nx">addAssets</span><span class="p">(</span><span class="s1">'Assets/**'</span><span class="p">);</span>
<span class="nx">project</span><span class="p">.</span><span class="nx">addSources</span><span class="p">(</span><span class="s1">'Sources'</span><span class="p">);</span>
<span class="nx">resolve</span><span class="p">(</span><span class="nx">project</span><span class="p">);</span>
</pre></div>

<h4>Sources/Main.hx</h4>
<div class="codehilite"><pre><span/><span class="kn">import</span> <span class="nn">kha</span><span class="p">.</span><span class="nn">System</span><span class="p">;</span>
<span class="kn">import</span> <span class="nn">kha</span><span class="p">.</span><span class="nn">Window</span><span class="p">;</span>

<span class="kd">class</span> <span class="n">Main</span> <span class="p">{</span>
    <span class="kd">public</span> <span class="kd">static</span> <span class="kd">function</span> <span class="nf">main</span><span class="p">()</span> <span class="p">{</span>
        <span class="kd">var</span> opt <span class="o">=</span> <span class="k">new</span> <span class="n">kha</span><span class="p">.</span><span class="n">System</span><span class="p">.</span><span class="n">SystemOptions</span><span class="p">(</span><span class="s2">&quot;My Kha Project&quot;</span><span class="p">,</span> <span class="mi">800</span><span class="p">,</span> <span class="mi">600</span><span class="p">);</span>
        <span class="n">System</span><span class="p">.</span><span class="n">start</span><span class="p">(</span><span class="n">opt</span><span class="p">,</span> <span class="n">onStart</span><span class="p">);</span>
    <span class="p">}</span>

    <span class="kd">public</span> <span class="kd">static</span> <span class="kd">function</span> <span class="nf">onStart</span><span class="p">(</span><span class="n">win</span><span class="p">:</span><span class="n">Window</span><span class="p">):</span><span class="n">Void</span><span class="p">{</span>
        <span class="k">new</span> <span class="n">Project</span><span class="p">();</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<h4>Sources/Project.hx</h4>
<div class="codehilite"><pre><span/><span class="kn">import</span> <span class="nn">kha</span><span class="p">.</span><span class="nn">System</span><span class="p">;</span>
<span class="kn">import</span> <span class="nn">kha</span><span class="p">.</span><span class="nn">Scheduler</span><span class="p">;</span>
<span class="kn">import</span> <span class="nn">kha</span><span class="p">.</span><span class="nn">Assets</span><span class="p">;</span>

<span class="kd">class</span> <span class="n">Project</span> <span class="p">{</span>
    <span class="kd">public</span> <span class="kd">function</span> <span class="nf">new</span><span class="p">()</span> <span class="p">{</span>
        <span class="n">Assets</span><span class="p">.</span><span class="n">loadEverything</span><span class="p">(</span><span class="n">onAssetsLoaded</span><span class="p">);</span>
    <span class="p">}</span>

    <span class="kd">public</span> <span class="kd">function</span> <span class="nf">onAssetsLoaded</span><span class="p">(){</span>
        <span class="n">System</span><span class="p">.</span><span class="n">notifyOnFrames</span><span class="p">(</span><span class="n">render</span><span class="p">);</span>
        <span class="n">Scheduler</span><span class="p">.</span><span class="n">addTimeTask</span><span class="p">(</span><span class="n">update</span><span class="p">,</span> <span class="mi">0</span><span class="p">,</span> <span class="mi">1</span> <span class="o">/</span> <span class="mi">60</span><span class="p">);</span>
    <span class="p">}</span>

    <span class="kd">public</span> <span class="kd">function</span> <span class="nf">update</span><span class="p">(){</span>
        <span class="c1">//game logic here</span>
    <span class="p">}</span>

    <span class="kd">public</span> <span class="kd">function</span> <span class="nf">render</span><span class="p">(</span><span class="n">framebuffers</span><span class="p">:</span><span class="n">Array</span><span class="p">&lt;</span><span class="n">Framebuffer</span><span class="p">&gt;){</span>
        <span class="c1">//render here</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<p>That's all you need for now. Haven't explained any of the code yet, but everything should be relatively straightforward so far. Only weird thing is the part about the asset loading. In short: Kha abstracts the bundling/loading of assets. This might seem like a bit of an annoying restriction, but it makes sense once you consider that Kha is designed to target native/C++ as well as HTML5/Javascript from one codebase.</p>
<h2>Building and Running The Project</h2>
<p>Building the project is actually pretty easy, just use this command:</p>
<div class="codehilite"><pre><span/>$ node path/to/Kha/make &lt;target&gt;
</pre></div>

<p>Where <code>&lt;target&gt;</code> is any supported target. If no target is given, it will build native for your platform. For example:</p>
<div class="codehilite"><pre><span/>$ node ~/repos/Kha/make html5
</pre></div>

<p>will generate the javascript to run your game, along with a basic HTML file to easily serve from the <code>build</code> directory.</p>
<h3>Targeting Krom</h3>
<p>Again, we want to use Krom because of the fast compile times. To do this is a two-step process. First, build the project using the 'krom' target:</p>
<div class="codehilite"><pre><span/>$ node ~/repos/Kha/make krom
</pre></div>

<p>When that completes, it will generate the folders <code>build/krom</code> and <code>build/krom-resources</code>. With that, we can run the game using the appropriate Krom binary that we checked out earlier:</p>
<div class="codehilite"><pre><span/>$ /path/to/Krom_bin/win32/Krom.exe /path/to/project/build/krom /path/to/project/build/krom-resources
</pre></div>

<p>If all went well, this should launch your recently-built project! If you're not using windows, of course you should use either the Linux or macOS binaries.</p>
<h3>Note about Linux</h3>
<p>The Krom_bin repository includes a binary for Linux, along with the shared libraries needed to run it. This means that you need to make sure the program loader finds those libraries. The easiest way that should work on most if not all Linux distros is to change the <code>LD_LIBRARY_PATH</code> variable to point to the Krom_bin/linux directory.</p>
<p>Example:</p>
<div class="codehilite"><pre><span/>$ <span class="nv">LD_LIBRARY_PATH</span><span class="o">=</span>/path/to/Krom_bin/linux /path/to/Krom_bin/linux/Krom /path/to/project/build/krom /path/to/project/build/krom-resources
</pre></div>

<h2>Using A Makefile</h2>
<p>Instead of typing out those commands every time, we can easily create a tiny Makefile to automate the process:</p>
<div class="codehilite"><pre><span/><span class="nv">pathKha</span><span class="o">=</span>/path/to/Kha
<span class="nv">pathKrom</span><span class="o">=</span>/path/to/Krom_bin
<span class="nv">krom</span><span class="o">=</span><span class="k">$(</span>pathKrom<span class="k">)</span>/win32/Krom.exe

<span class="nf">build</span><span class="o">:</span> .<span class="n">PHONY</span>
    node <span class="k">$(</span>pathKha<span class="k">)</span>/make krom

<span class="nf">run</span><span class="o">:</span> <span class="n">build</span>
    <span class="k">$(</span>krom<span class="k">)</span> build/krom build/krom-resources

<span class="nf">.PHONY</span><span class="o">:</span>
</pre></div>

<p>If on linux, you may want to replace the variable <code>krom</code> with something like this:</p>
<div class="codehilite"><pre><span/><span class="nv">krom</span><span class="o">=</span><span class="nv">LD_LIBRARY_PATH</span><span class="o">=</span><span class="k">$(</span>pathKrom<span class="k">)</span>/linux <span class="k">$(</span>pathKrom<span class="k">)</span>/linux/Krom
</pre></div>

<p>Just save that as a file called <code>Makefile</code> in your project directory (next to the <code>khafile.js</code>), and then run</p>
<div class="codehilite"><pre><span/>$ make
</pre></div>

<p>to build it, or</p>
<div class="codehilite"><pre><span/>$ make run
</pre></div>

<p>to build and run it in Krom!</p>
<h2>Next Steps</h2>
<p>That's all you need to do to start working with Kha if you don't want to use KodeStudio. I personally like to use SublimeText3 for my projects, and having a Makefile enables me to build and run from within the editor by pressing CTRL+B.</p>
<p>After this, I'll make another short tutorial showing how to actually use some Kha APIs to draw stuff on-screen.</p></span><div class="series-nav">
	<a href="">  </a>
	<a href="index.html">(Index)</a>
	<a href="kha-tutorial-2.html"> Next </a>
</div><p class="copyright">© Alejandro Ramallo 2021</p></article>
		</div>
	</div>
	</body>
</html>