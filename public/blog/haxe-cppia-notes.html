<?xml version="1.0" ?><html>
	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"/>
		<link rel="stylesheet" href="/source/style.css"/>
		<link rel="stylesheet" href="/syntax-style.css"/>
		<title>AlexRamallo | Haxe Cppia Notes</title>
	<meta property="og:title" content="Haxe Cppia Notes"/><meta property="og:description" content="Some notes about hxcpp's Cppia target"/><meta property="og:url" content="https://aramallo.com/haxe-cppia-notes.html"/></head>
	<body>
		<div class="menu-tag-2 menu-item">
			AlexRamallo
			 &lt;<a href="mailto:alejandro@ramallo.me">alejandro@ramallo.me</a>&gt;
			 &lt;<a href="https://aramallo.itch.io/">Itch</a>&gt;
			 &lt;<a href="https://twitter.com/AlexRamallo">Twitter</a>&gt;
			 &lt;<a href="/blog/rss.xml">RSS</a>&gt;
		</div>

		<div class="body-wrapper">
		<div class="menu-bar">
			<div id="NavMenu" class="menu-bar-body"><a class="menu-item" href="/">Home</a><a class="menu-item" href="/projects">Projects</a><a class="menu-item" href="/blog">Blog</a><a class="menu-item" href="/About">About</a></div>
		</div>
		<div class="menu-tag-1">
		AlexRamallo &lt;<a href="mailto:alejandro@ramallo.me">alejandro@ramallo.me</a>&gt;
		&lt;<a href="/blog/rss.xml">RSS</a>&gt;
		</div>
		<div class="main-content">
			<article id="MainContent" class="main-content-body"><span><h1>Haxe Cppia Notes</h1>
<p class="article-date">Tuesday, June 18, 2019 - 12:00 AM</p>

<h2>Overview of Cppia</h2>
<p>Cppia is an additional target for the Haxe compiler which depends on the Hxcpp runtime. On the Hxcpp side, you have a virtual machine which can interpret Cppia bytecode. Cppia bytecode is generated from standard Haxe code using the Haxe compiler just like any other backend. There isn't really any magic involved here as far as I can tell, it's just different from most other targets because it's tightly coupled to the Hxcpp backend.</p>
<p>One of the coolest benefits of Cppia is that it's a system target (so it has access to the same APIs as the Cpp target), and it compiles extremely quickly. This lets you do rapid iteration on a native target, much like the Neko or Hashlink targets. However, unlike Neko and Hashlink, you can load and execute Cppia code during runtime from your Hxcpp application very easily. This can be used for scripting, for example.</p>
<p>If you were to try to implement scripting like that using Neko or Hashlink, you would need to embed a Neko or Hashlink virtual machine into your program manually, and maybe add a way to load/execute those scripts from your Haxe code. This is a totally viable option of course (and the Hashlink target might even perform better than Cppia <em>[this is unconfirmed]</em>), but it's a lot of work. With Cppia, all you have to do is add the <code>-D scriptable</code> flag to your host application (which causes the Cppia interpreter to be compiled), and then use the <code>cpp.cppia.*</code> APIs in the standard library to load/run/reflect on your scripts.</p>
<p>Another benefit is that since Cppia is compiled from standard Haxe code, you could use Cppia during development to iterate rapidly, and then generate C++ code when its time to ship so that you get maximum performance and avoid having to include the Cppia interpreter in your final build.</p>
<h2>Using Cppia</h2>
<h3>Compiling Cppia scripts</h3>
<p>To compile Haxe code as Cppia, you use the <code>-cppia</code> flag. For example:</p>
<div class="codehilite"><pre><span/>haxe -main MyScript.hx -cppia ./build/myscript.cppia
</pre></div>

<p>This will generate <code>myscript.cppia</code> in a folder called <code>build</code>. Since this is a standard Haxe program, <code>MyScript</code> should have a <code>public static function main():Void</code> function.</p>
<h3>Running Cppia scripts</h3>
<p>To run a Cppia script, you need a <strong>Cppia Host</strong>. This is any hxcpp program that includes the Cppia virtual machine. To create one, you just need to add the <code>-D scriptable</code> definition to the Haxe compiler when using the hxcpp target. Example:</p>
<div class="codehilite"><pre><span/>haxe -main MyHost.hx -D scriptable -cpp ./build
</pre></div>

<p>This will compile the program with support for running Cppia scripts, but you still have to run them yourself somehow in your code. Simply adding the <code>scriptable</code> flag won't automatically turn your program into a command-line script runner. To run a Cppia script, you have to read the contents (as a text file), and use it with the <code>cpp.cppia.*</code> APIs. For example:</p>
<div class="codehilite"><pre><span/><span class="kn">import</span> <span class="nn">cpp</span><span class="p">.</span><span class="nn">cppia</span><span class="p">.</span><span class="nn">Host</span><span class="p">;</span>

<span class="kd">class</span> <span class="n">MyHost</span> <span class="p">{</span>
    <span class="kd">public</span> <span class="kd">static</span> <span class="kd">function</span> <span class="nf">main</span><span class="p">():</span><span class="n">Void</span><span class="p">{</span>
        <span class="kd">var</span> src<span class="p">:</span><span class="n">String</span> <span class="o">=</span> <span class="n">sys</span><span class="p">.</span><span class="n">io</span><span class="p">.</span><span class="n">File</span><span class="p">.</span><span class="n">getContent</span><span class="p">(</span><span class="s2">&quot;myscript.cppia&quot;</span><span class="p">);</span> <span class="c1">//load script contents into string</span>
        <span class="n">Host</span><span class="p">.</span><span class="n">run</span><span class="p">(</span><span class="n">src</span><span class="p">);</span> <span class="c1">//automatically initialize and run the entrypoint (static main function)</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<p>And that's all there is to it. If you need more control, like reflection on types in the script, you can use the <code>cpp.cppia.Module</code> class. <a href="https://github.com/HaxeFoundation/haxe/blob/3.4.7/std/cpp/cppia/Host.hx">See the source</a> for the <code>cpp.cppia.Host.run</code> function for an example of how to initialize and run a Module instance. It's super simple, and then once you have an instance you can use the function <code>cpp.cppia.Module.resolveClass</code> to find types defined in the script. Those types can then be used with Haxe's standard <code>Type.createInstance</code> and similar functions.</p>
<h2>Using Libraries in Cppia</h2>
<p>Cppia scripts are standard Haxe, so you could <em>probably</em> use any existing Haxe library with it. You could probably even use OpenFL, although OpenFL is a huge library and you'd be better off keeping that in your host rather than distributing it in every single script.</p>
<p>But then this raises the question: <strong>how do you use library types in a Cppia script if you don't want to compile that library along with your script?</strong></p>
<p>The answer is that Cppia offers a way to exclude classes that are already in the Host. So you're free to add any haxelib to your cppia script using the standard <code>-lib</code> flag. This makes those types available to you within the script, but doesn't actually compile them. It will assume that those types are available in the Host, so they will try to be resolved at runtime.</p>
<p>So how does this work? When you compile your host with the <code>-D scriptable</code> flag, the compiler will generate a file called <strong>export_classes.info</strong>. This contains a list of every class included in the Host. You can use this file when compiling your Cppia script to inform the compiler as to which classes can be safely excluded from the compilation.</p>
<h3>How to exclude classes with export_classes.info</h3>
<p>The Cppia backend includes a macro which will automatically search for a file called <code>export_classes.info</code> <strong>in the classpath</strong>. That part is in bold because it is important: the file MUST be in your classpath (a folder can be added to the classpath with the <code>-cp</code> flag)</p>
<p>Don't assume that the Host will generate that file in the correct place, because it probably won't. You need to make sure that you copy that file into the classpath of your Cppia script. You could automate the process of copying <code>export_classes.info</code> into the correct place if desired.</p>
<p>An alternative method is to not rely on that search function and just manually specify the path to that file. This can be done with the <code>-D dll_import=path/to/file.info</code> flag. You would need to add that flag to your Cppia compile command.</p>
<h4>Fin</h4>
<p>If you see any errors here or have comments, feel free to contact me via email or Twitter. </p>
<h4>Sources</h4>
<p><a href="https://groups.google.com/forum/#!topic/haxelang/yURC8k2fGeg">https://groups.google.com/forum/#!topic/haxelang/yURC8k2fGeg</a></p>
<p><a href="https://stackoverflow.com/questions/30008574/what-is-cppia-scripting">https://stackoverflow.com/questions/30008574/what-is-cppia-scripting</a></p>
<p><a href="https://code.haxe.org/category/other/working-with-cppia/index.html">https://code.haxe.org/category/other/working-with-cppia/index.html</a></p></span><p class="copyright">© Alejandro Ramallo 2021</p></article>
		</div>
	</div>
	</body>
</html>